from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from .scripts import dashboard
import requests
import random


@login_required(login_url="login")
def home(request):
    dict = dashboard.generate_dashboard_data()
    req = requests.get(f'https://pokeapi.co/api/v2/pokemon/{random.randint(1, 151)}').json()
    req2 = requests.get("https://api.chucknorris.io/jokes/random").json()
    context = {
        "data": dict["data"],
        "pikachu": req["sprites"]["front_default"],
        "norris": req2["value"]
    }
    return render(request, 'index.html', context=context)


def login_view(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('home')
    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('login')